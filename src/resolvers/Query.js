async function feed(parent, args, context, info) {
    const where = args.filter ? {
        OR: [
            { title_contains: args.filter },
            { description_contains: args.filter },
            { city_contains: args.filter },
        ],
    } : {}

    const jobs = await context.prisma.jobs({
        where,
        skip: args.skip,
        first: args.first,
        orderBy: args.orderBy
    })
    const count = await context.prisma.jobsConnection({
        where,
    })
        .aggregate()
        .count()

    return {
        jobs,
        count,
    }
}

module.exports = {
    feed,
}