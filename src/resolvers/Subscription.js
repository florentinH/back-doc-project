function newJobSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.job({ mutation_in: ['CREATED'] }).node()
}

const newJob = {
    subscribe: newJobSubscribe,
    resolve: payload => {
        return payload
    },
}

module.exports = {
    newJob,
}