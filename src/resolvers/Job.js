function postedBy(parent, args, context) {
    return context.prisma.job({ id: parent.id }).postedBy()
}

module.exports = {
    postedBy,
}