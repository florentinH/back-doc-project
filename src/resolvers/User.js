function jobs(parent, args, context) {
    return context.prisma.user({ id: parent.id }).jobs()
}

module.exports = {
    jobs,
}